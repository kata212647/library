package com.kata.library.cucumber;

import com.fasterxml.jackson.databind.JavaType;
import io.cucumber.java.DefaultDataTableCellTransformer;
import io.cucumber.java.DefaultDataTableEntryTransformer;
import io.cucumber.java.DefaultParameterTransformer;
import io.cucumber.spring.CucumberContextConfiguration;

import java.lang.reflect.Type;


@CucumberContextConfiguration
public class CommonStepDef extends ContextLoader {

    /**
     * Defines a default conversion from DataTable to Java Objects
     *
     * @param fromValue
     * @param toValueType
     * @return
     */
    @DefaultParameterTransformer
    @DefaultDataTableEntryTransformer
    @DefaultDataTableCellTransformer
    public Object defaultTransformer(Object fromValue, Type toValueType) {
        JavaType javaType = mapper.constructType(toValueType);
        return mapper.convertValue(fromValue, javaType);
    }
}
