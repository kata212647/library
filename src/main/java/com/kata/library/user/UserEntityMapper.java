package com.kata.library.user;


import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface UserEntityMapper {

    User convert(UserEntity userEntity);

}
