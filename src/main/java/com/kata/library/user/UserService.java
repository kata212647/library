package com.kata.library.user;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final UserEntityMapper userEntityMapper;

    public UserService(UserRepository userRepository, UserEntityMapper userEntityMapper) {
        this.userRepository = userRepository;
        this.userEntityMapper = userEntityMapper;
    }

    List<User> getUsers() {
        return userRepository.findAll()
                .stream().map(userEntityMapper::convert)
                .collect(Collectors.toList());
    }
}
