package com.kata.library.swagger;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.models.OpenAPI;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@OpenAPIDefinition(
        info = @Info(
                title = "Library management API",
                version = "${info.app.version}")
)
public class SwaggerConfig {


    @Bean
    public OpenAPI openAPI() {
        return new OpenAPI();
    }

}
