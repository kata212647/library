package com.kata.library.book;

import java.time.LocalDate;
import java.util.UUID;

public record Book(

        UUID id,

        String title

) {
}
