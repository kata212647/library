# Library Management System

## Overview
This exercise involves the development of a system for managing book loans in a library. 

The system will allow to :
- register users
- add books to the library
- loan available books.

## Objectives
Implement a system that allows individuals to loan books from a library.

Ensure the system adheres to the following rules:
A person must be registered with the library to loan a book.
A book must be available for loan (i.e., it should be part of the library's collection and not currently rented out).

## Requirements
Java Development Kit (JDK): Ensure you have JDK 17 installed on your system.

Integrated Development Environment (IDE): Any Java IDE like IntelliJ IDEA, Eclipse, or NetBeans.

Maven: For dependency management and building the project.

Git: For version control.

Docker: For running a local database

## Setup Instructions

### Build
Clone the Repository.

Navigate to the Project Directory.

Build the Project:

`mvn clean install`

### Database
In order to start a local postgres database with docker :

`cd docker/local-env-kata-library`

`docker-compose up`


## Project Dependencies
### Swagger
This project uses swagger for API documentation.

After starting the SpringBoot application, the swagger doc is available on this url : `http://localhost:8080/swagger-ui/index.html`

### Liquibase
Use liquibase for database schema change management (Have a look at resources/db/changelog directory).

### Cucumber
First test use cases have been implemented with cucumber (Have a look at test/resources/features directory).

If you're not familiar with cucumber, you can use a testing tool of your choice.


## Tasks

### 1. User registration
Create new endpoint allowing to register new user.

Note that user email has to be unique

### 2. Add new book
Create new endpoint allowing to add new book to the library.

### 3. Search a book by title
Enrich the existing endpoint with a filter allowing to search books by title.

Note that the search has to be case-insensitive 

### 4. Retrieve available books
Enrich the existing endpoint with a filter allowing to retrieve books available for loan.

In order to implement this step, you need to consider the loan modeling.

Note that a loan should include one user, one book, a start date and an end date.

### 5. Loan a book
Create new endpoint allowing to loan a book.

Note that a loan can be proceeded only if 
- the user is registered
- the book is available for loan

## Submission
You can submit your code directly in this repository by creating a dedicated branch.

Ensure your code follows good coding practices.